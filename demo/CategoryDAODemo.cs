﻿using System;
using System.Collections.Generic;

namespace OOP.demo
{
    class CategoryDAODemo
    {

        /**
         * Test whether a category object is stored in the database or not.
         * @return void
         */
        public void insertTest()
        {
            entity.Category categoryTest = new entity.Category(1, "Monitor");
            if (new dao.CategoryDAO().Insert(categoryTest) == 1)
            {
                Console.WriteLine("Insert category successfully.");
            }
            else { Console.WriteLine("Failed to insert category."); }
        }

        /**
         * Test whether a category object is updated to the database or not.
         * @return void
         */
        public void updateTest()
        {
            entity.Category categoryTest = new entity.Category(1, "KeyBoard");
            if (new dao.CategoryDAO().Update(categoryTest) == 1)
            {
                Console.WriteLine("Update category successfully.");
            }
            else { Console.WriteLine("Failed to update category."); }
        }

        /**
         * Test whether a category object is deleted from the database or not.
         * @return void
         */
        public void deleteTest()
        {
            entity.Category categoryTest = new entity.Category(1, "KeyBoard");
            if (new dao.CategoryDAO().Delete(categoryTest))
            {
                Console.WriteLine("Delete category successfully.");
            }
            else { Console.WriteLine("Failed to delete category."); }
        }

        /**
         * Test whether list of categores in the database is printed or not.
         * @return void
         */
        public void findAllTest()
        {
            List<entity.Category> categoryFound = new dao.CategoryDAO().FindAll();

            Console.WriteLine("Category ID\tCategory Name");
            for (int i = 0; i < categoryFound.Count; i++)
            {
                Console.WriteLine(categoryFound[i].Id + "\t" + categoryFound[i].name);
            }
        }

        /**
         * Test whether list of categores in the database is printed by id or not.
         * @return void
         */
        public void findByIdTest()
        {
            int idTest = 3;

            entity.Category categoryFound = new dao.CategoryDAO().FindById(idTest);

            Console.WriteLine("Category ID\tCategory Name");
            Console.WriteLine(categoryFound.Id + "\t" + categoryFound.name);
        }
    }
}

﻿using System;

namespace OOP.demo
{
    class DatabaseDemo
    {

        /**
         * Test whether an object is stored in the specific table or not.
         * @return void
         */
        public void insertTableTest()
        {
            entity.Product productTest = new entity.Product(1, "Samsung GS20", 3);
            dao.Database.Instance().insertTable("product", productTest);

            entity.Category categoryTest = new entity.Category(4, "Watch");
            dao.Database.Instance().insertTable("category", categoryTest);

            entity.Accessory accessoryTest = new entity.Accessory(1, "Headset", 10);
            dao.Database.Instance().insertTable("accessory", accessoryTest);
        }

        /**
         * Test whether a list of specific object is printed or not.
         * @return void
         */
        public void selectTableTest()
        {
            string[] testInput = { "product", "category", "accessory", "e" };

            dynamic selectProductTable = dao.Database.Instance().selectTable("product");
            dynamic selectCategotyTable = dao.Database.Instance().selectTable("category");
            dynamic selectAccessoryTable = dao.Database.Instance().selectTable("accessory");
            dynamic wrongInput = dao.Database.Instance().selectTable("e");


            Console.WriteLine("Product ID\tProduct Name\t Category ID");
            for (int i = 0; i < selectProductTable.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}", selectProductTable[i].Id, selectProductTable[i].name, selectProductTable[i].categotyId);
            }

            Console.WriteLine();

            Console.WriteLine("Category ID\tCategory Name");
            for (int i = 0; i < selectCategotyTable.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}", selectCategotyTable[i].Id, selectCategotyTable[i].name);
            }

            Console.WriteLine();

            Console.WriteLine("Accessory ID\tAccessory Name\tAccessory ID");
            for (int i = 0; i < selectAccessoryTable.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}", selectAccessoryTable[i].Id, selectAccessoryTable[i].name, selectAccessoryTable[i].categotyId);
            }

            Console.ReadLine();
        }

        /**
         * Test whether an object is updated to the specific table or not.
         * @return void
         */
        public void updateTableTest()
        {
            entity.Product productTest = new entity.Product(1, "Lenovo X2", 3);
            dao.Database.Instance().updateTable("product", productTest);

            entity.Product anotherProductTest = new entity.Product(1, "Dell X", 2);
            dao.Database.Instance().updateTableById(8, productTest);
        }

        /**
         * Test whether an object is deleted from the specific table or not.
         * @return void
         */
        public void deleteTableTest()
        {
            entity.Product productTest = new entity.Product(1, "Lenovo X2", 3);
            dao.Database.Instance().deleteTable("product", productTest);
        }

        /**
         * Test whether all data in a specific table is deleted or not.
         * @return void
         */
        public void truncateTableTest()
        {
            dao.Database.Instance().truncateTable("product");
            dao.Database.Instance().truncateTable("category");
            dao.Database.Instance().truncateTable("accessory");
            dao.Database.Instance().truncateTable("e");
        }

        /**
         * Test by creating a test database.
         * @return void
         */
        public void initDatabase()
        {
            dao.Database databaseTest = dao.Database.Instance();

            databaseTest.productTable.Add(new entity.Product(1, "Iphone 6", 5));
            databaseTest.productTable.Add(new entity.Product(2, "Intel i8", 8));
            databaseTest.productTable.Add(new entity.Product(3, "Nokia A60", 3));
            databaseTest.productTable.Add(new entity.Product(4, "Samsung GNote 8", 5));
            databaseTest.productTable.Add(new entity.Product(5, "G-Shock", 4));
            databaseTest.productTable.Add(new entity.Product(6, "Asus X50", 1));
            databaseTest.productTable.Add(new entity.Product(7, "Apple watch s2", 4));
            databaseTest.productTable.Add(new entity.Product(8, "IPhone X", 5));
            databaseTest.productTable.Add(new entity.Product(9, "Huion H420", 9));
            databaseTest.productTable.Add(new entity.Product(10, "Galaxy Watch S2", 4));

            databaseTest.categoryTable.Add(new entity.Category(1, "PC"));
            databaseTest.categoryTable.Add(new entity.Category(2, "Laptop"));
            databaseTest.categoryTable.Add(new entity.Category(3, "Phone"));
            databaseTest.categoryTable.Add(new entity.Category(4, "Clock"));
            databaseTest.categoryTable.Add(new entity.Category(5, "Smartphone"));
            databaseTest.categoryTable.Add(new entity.Category(6, "CPU"));
            databaseTest.categoryTable.Add(new entity.Category(7, "RAM"));
            databaseTest.categoryTable.Add(new entity.Category(8, "Motherboard"));
            databaseTest.categoryTable.Add(new entity.Category(9, "Tablet"));
            databaseTest.categoryTable.Add(new entity.Category(10, "Accessory"));

            databaseTest.accessoryTable.Add(new entity.Accessory(1, "Hukey earbub", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(2, "Kingston 128GB", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(3, "Siyoteam SY", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(4, "Logitech 2000", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(5, "Kingston 32GB", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(6, "Kingston 256GB", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(7, "Galaxy earbub S3", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(8, "Apple Airpod", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(9, "Samsumg 30W Charging Box", 10));
            databaseTest.accessoryTable.Add(new entity.Accessory(10, "USB fan", 10));

            Console.ReadLine();
        }

        /**
         * Test whether a specific table is printed or not.
         * @return void
         */
        public void printTableTest()
        {
            dao.Database database = dao.Database.Instance();

            Console.WriteLine("Table of product:");
            Console.WriteLine("ID\tName\tCategoryID");
            for (int i = 0; i < database.productTable.Count; i++)
            {
                Console.WriteLine(database.productTable[i].Id + "\t" + database.productTable[i].name + "\t" + database.productTable[i].categotyId);
            }

            Console.WriteLine();

            Console.WriteLine("Table of category:");
            Console.WriteLine("ID\tName");
            for (int i = 0; i < database.productTable.Count; i++)
            {
                Console.WriteLine(database.categoryTable[i].Id + "\t" + database.categoryTable[i].name);
            }

            Console.WriteLine();

            Console.WriteLine("Table of accessory:");
            Console.WriteLine("ID\tName\tCategoryID");
            for (int i = 0; i < database.accessoryTable.Count; i++)
            {
                Console.WriteLine(database.accessoryTable[i].Id + "\t" + database.accessoryTable[i].name + "\t" + database.accessoryTable[i].categotyId);
            }

            Console.ReadLine();
        }
    }
}

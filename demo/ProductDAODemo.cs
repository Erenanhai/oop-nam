﻿using System;
using System.Collections.Generic;

namespace OOP.demo
{
    class ProductDAODemo
    {
        /**
         * Test whether a product object is stored in the database or not.
         * @return void
         */
        public void insertTest()
        {
            entity.Product ProductTest = new entity.Product(1, "Samsung A20", 3);
            if (new dao.ProductDAO().Insert(ProductTest) == 1)
            {
                Console.WriteLine("Insert Product successfully.");
            }
            else { Console.WriteLine("Failed to insert Product."); }
        }

        /**
         * Test whether a product object is updated to the database or not.
         * @return void
         */
        public void updateTest()
        {
            entity.Product ProductTest = new entity.Product(1, "Asus X8", 1);
            if (new dao.ProductDAO().Update(ProductTest) == 1)
            {
                Console.WriteLine("Update Product successfully.");
            }
            else { Console.WriteLine("Failed to update Product."); }
        }

        /**
         * Test whether a product object is deleted from the database or not.
         * @return void
         */
        public void deleteTest()
        {
            entity.Product ProductTest = new entity.Product(1, "Samsung A20", 3);
            if (new dao.ProductDAO().Delete(ProductTest))
            {
                Console.WriteLine("Delete Product successfully.");
            }
            else { Console.WriteLine("Failed to delete Product."); }
        }

        /**
         * Test whether list of products in the database is printed or not.
         * @return void
         */
        public void findAllTest()
        {
            List<entity.Product> ProductFound = new dao.ProductDAO().FindAll();

            Console.WriteLine("Product ID\tProduct Name\tProduct Category ID");
            for (int i = 0; i < ProductFound.Count; i++)
            {
                Console.WriteLine(ProductFound[i].Id + "\t" + ProductFound[i].name + "\t" + ProductFound[i].categotyId);
            }
        }

        /**
         * Test whether list of products in the database is printed by id or not.
         * @return void
         */
        public void findByIdTest()
        {
            int idTest = 5;

            entity.Product ProductFound = new dao.ProductDAO().FindById(idTest);

            Console.WriteLine("Product ID\tProduct Name\tProduct Category ID");
            Console.WriteLine(ProductFound.Id + "\t" + ProductFound.name + "\t" + ProductFound.categotyId);
        }

        /**
         * Test whether list of products in the database is printed by name or not.
         * @return void
         */
        public void findByNameTest()
        {
            string nameTest = "Samsung A20";

            entity.Product ProductFound = new dao.ProductDAO().FindByName(nameTest);

            Console.WriteLine("Product ID\tProduct Name\tProduct Category ID");
            Console.WriteLine(ProductFound.Id + "\t" + ProductFound.name + "\t" + ProductFound.categotyId);
        }

        /**
         * Test whether list of products in the database is printed by category id or not.
         * @return void
         */
        public void searchTest()
        {
            int categoryIDTest = 3;

            List<entity.Product> ProductFound = new dao.ProductDAO().search(categoryIDTest);

            Console.WriteLine("Product ID\tProduct Name\tProduct Category ID");
            for (int i = 0; i < ProductFound.Count; i++)
            {
                Console.WriteLine(ProductFound[i].Id + "\t" + ProductFound[i].name + "\t" + ProductFound[i].categotyId);
            }
        }
    }
}

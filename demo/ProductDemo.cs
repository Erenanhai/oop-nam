﻿using System;

namespace OOP.demo
{
    class ProductDemo
    {
        /**
         * Create test product by product name, id, category id
         * @return mixed
         */
        public OOP.entity.Product CreateProductTest()
        {
            Console.WriteLine("Enter product name:");
            string productName = Console.ReadLine();
            Console.WriteLine("Enter product id:");
            int productId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter catagory id of the product:");
            int categotyId = Convert.ToInt32(Console.ReadLine());

            return new OOP.entity.Product(productId, productName, categotyId);
        }
        /**
         * Print test product id, name, category id
         * @return void
         */
        public void PrintProduct(OOP.entity.Product product)
        {
            Console.WriteLine("ID\tName\tCategoryID");
            Console.WriteLine(product.Id + "\t" + product.name + "\t" + product.categotyId);
        }
    }
}

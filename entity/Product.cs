﻿namespace OOP.entity
{
    public class Product : BaseRow
    {
        public int categotyId;

        /**
         * Set product informaiton (id, name, category id)
         * @param product id
         * @param product name
         * @param product category id
         */
        public Product(int productId, string productName, int productCatagoryId)
        {
            Id = productId;
            name = productName;
            categotyId = productCatagoryId;
        }

        public override int Id
        {
            get { return id; }

            set
            {
                id = Id;
            }
        }


    }
}

﻿namespace OOP.entity
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
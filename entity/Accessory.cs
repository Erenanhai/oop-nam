﻿namespace OOP.entity
{
    class Accessory : BaseRow
    {
        public int categotyId;

        /**
         * Set accessory informaiton (id, name, category id)
         * @param accessory id
         * @param accessory name
         * @param accessory category id
         */
        public Accessory(int accessoryId, string accessoryName, int accessoryCategoryId)
        {
            Id = accessoryId;
            name = accessoryName;
            categotyId = accessoryCategoryId;
        }

        public override int Id
        {
            get { return id; }
            set { id = Id; }
        }
    }
}

﻿namespace OOP.entity
{
    class Category : BaseRow
    {
        /**
         * Set category informaiton (id, name)
         * @param category id
         * @param category name
         */
        public Category(int categoryId, string categoryName)
        {
            Id = categoryId;
            name = categoryName;
        }

        public override int Id
        {
            get { return id; }
            set { id = Id; }
        }
    }
}

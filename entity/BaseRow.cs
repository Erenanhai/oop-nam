﻿namespace OOP.entity
{
    public abstract class BaseRow : IEntity
    {
        public string name;
        protected int id;

        public abstract int Id { get; set; }
    }
}
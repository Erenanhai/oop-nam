﻿using System.Collections.Generic;

namespace OOP.dao
{
    class ProductDAO : BaseDao
    {
        /**
         * Store a product to the database
         * @param row
         * return true/false
         */
        public override int Insert(dynamic row)
        {
            Database database = Database.Instance();
            database.insertTable("product", row);
            return 1;
        }

        /**
         * Update a product in the database
         * @param row
         * return true/false
         */
        public override int Update(dynamic row)
        {
            Database database = Database.Instance();
            database.updateTable("product", row);
            return 1;
        }

        /**
         * Delete a product from the database
         * @param row
         * return true/false
         */
        public override bool Delete(dynamic row)
        {
            Database database = Database.Instance();
            database.insertTable("product", row);
            return true;
        }

        /**
         * Get a list of products in the database
         * return list
         */
        public override dynamic FindAll()
        {
            Database database = Database.Instance();
            return database.selectTable("product"); ;
        }

        /**
         * Get a list of products in the database filtered by id
         * return list
         */
        public override dynamic FindById(int id)
        {
            Database database = Database.Instance();

            List<entity.Product> productList = database.selectTable("product");
            for (int i = 0; i < productList.Count; i++)
            {
                if (productList[i].Id == id)
                {
                    return productList[i];
                }
            }
            return null;
        }

        /**
         * Get a list of products in the database filtered by name
         * return list
         */
        public dynamic FindByName(string name)
        {
            Database database = Database.Instance();

            List<entity.Product> productList = database.selectTable("product");
            for (int i = 0; i < productList.Count; i++)
            {
                if (productList[i].name == name)
                {
                    return productList[i];
                }
            }
            return null;
        }

        /**
         * Get a list of products in the database filtered by category id
         * return list
         */
        public dynamic search(int categoryId)
        {
            Database database = Database.Instance();

            List<entity.Product> productList = database.selectTable("product");

            List<entity.Product> FoundProductList = new List<entity.Product>();

            for (int i = 0; i < productList.Count; i++)
            {
                if (productList[i].categotyId == categoryId)
                {
                    FoundProductList.Add(productList[i]);
                }
            }
            return FoundProductList;
        }
    }
}

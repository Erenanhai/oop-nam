﻿using System;
using System.Collections.Generic;

namespace OOP.dao
{
    class Database
    {
        public List<OOP.entity.Product> productTable;
        public List<OOP.entity.Category> categoryTable;
        public List<OOP.entity.Accessory> accessoryTable;
        public static Database instants;

        /**
         * Initialize database
         * @return database
         */
        public static Database Instance()
        {
            if (instants == null)
            {
                instants = new Database();
            }
            return instants;
        }

        /**
         * Store record in table
         * @param name
         * @param row
         * @return void
         */
        public int insertTable(string name, dynamic row)
        {

            switch (name)
            {
                case "product":
                    productTable.Add(row);
                    break;
                case "category":
                    categoryTable.Add(row);
                    break;
                case "accessory":
                    accessoryTable.Add(row);
                    break;
                default:
                    Console.WriteLine("Please choose between Product, Category or Accessory");
                    break;
            }

            return 0;
        }

        /**
         * Choose a table
         * @param name
         * @return mixed
         */
        public dynamic selectTable(string name)
        {
            switch (name)
            {
                case "product":
                    return productTable;
                case "category":
                    return categoryTable;
                case "accessory":
                    return accessoryTable;
                default:
                    Console.WriteLine("Please choose between Product, Category or Accessory");
                    return null;
            }
        }

        /**
         * Update record in table
         * @param name
         * @param row
         * @return void
         */
        public int updateTable(string name, dynamic row)
        {
            switch (name)
            {
                case "product":
                    for (int i = 0; i < productTable.Count; i++)
                    {
                        if (productTable[i].Id == row.Id)
                        {
                            productTable[i] = row;
                        }
                    }
                    break;
                case "category":
                    for (int i = 0; i < categoryTable.Count; i++)
                    {
                        if (categoryTable[i].Id == row.Id)
                        {
                            categoryTable[i] = row;
                        }
                    }
                    break;
                case "accessory":
                    for (int i = 0; i < accessoryTable.Count; i++)
                    {
                        if (accessoryTable[i].Id == row.Id)
                        {
                            accessoryTable[i] = row;
                        }
                    }
                    break;
                default:
                    Console.WriteLine("Please choose between Product, Category or Accessory");
                    break;
            }

            return 0;
        }

        /**
         * Delete record in table
         * @param name
         * @param row
         * @return void
         */
        public int deleteTable(string name, OOP.entity.Product row)
        {
            switch (name)
            {
                case "product":
                    for (int i = 0; i < productTable.Count; i++)
                    {
                        if (productTable[i].Id == row.Id)
                        {
                            productTable.RemoveAt(i);
                        }
                    }
                    break;
                case "category":
                    for (int i = 0; i < categoryTable.Count; i++)
                    {
                        if (categoryTable[i].Id == row.Id)
                        {
                            categoryTable.RemoveAt(i);
                        }
                    }
                    break;
                case "accessory":
                    for (int i = 0; i < accessoryTable.Count; i++)
                    {
                        if (accessoryTable[i].Id == row.Id)
                        {
                            accessoryTable.RemoveAt(i);
                        }
                    }
                    break;
                default:
                    Console.WriteLine("Please choose between Product, Category or Accessory");
                    break;
            }
            return 0;
        }

        /**
         * Delete all records in table
         * @param name
         * @return void
         */
        public void truncateTable(string name)
        {
            switch (name)
            {
                case "product":
                    productTable.Clear();
                    break;
                case "category":
                    categoryTable.Clear();
                    break;
                case "accessory":
                    accessoryTable.Clear();
                    break;
                default:
                    Console.WriteLine("Please choose between Product, Category or Accessory");
                    break;
            }
        }

        /**
         * Update record by id in table
         * @param id
         * @param row
         * @return void
         */
        public int updateTableById(int id, OOP.entity.Product row)
        {
            for (int i = 0; i < productTable.Count; i++)
            {
                if (productTable[i].Id == id)
                {
                    productTable[i] = row;
                }
            }
            return 0;
        }
    }
}

﻿using System.Collections.Generic;

namespace OOP.dao
{
    class CategoryDAO : BaseDao
    {
        /**
         * Delete record in category table
         * @param row
         * @return true/false
         */
        public override bool Delete(dynamic row)
        {
            Database database = Database.Instance();
            database.insertTable("category", row);
            return true;
            //throw new System.NotImplementedException();
        }

        /**
         * Get all records in category table
         * @return list
         */

        public override dynamic FindAll()
        {
            Database database = Database.Instance();
            return database.selectTable("category");
            //throw new System.NotImplementedException();
        }

        /**
         * Get records by id in category table
         * @param id
         * @return list
         */

        public override dynamic FindById(int id)
        {
            Database database = Database.Instance();

            List<entity.Category> categoriesList = database.selectTable("category");
            for (int i = 0; i < categoriesList.Count; i++)
            {
                if (categoriesList[i].Id == id)
                {
                    return categoriesList[i];
                }
            }
            return null;
            //throw new System.NotImplementedException();
        }

        /**
         * Store record in category table
         * @param row
         * @return void
         */

        public override int Insert(dynamic row)
        {
            Database database = Database.Instance();
            database.insertTable("category", row);
            return 1;
            //throw new System.NotImplementedException();
        }

        /**
         * Update record in category table
         * @param row
         * @return void
         */
        public override int Update(dynamic row)
        {
            Database database = Database.Instance();
            database.updateTable("category", row);
            return 1;
            throw new System.NotImplementedException();
        }
    }
}

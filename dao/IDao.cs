﻿namespace OOP.dao
{
    internal interface IDao
    {
        bool Delete(dynamic row);
        dynamic FindAll();
        dynamic FindById(int id);
        int Insert(dynamic row);
        int Update(dynamic row);
    }
}
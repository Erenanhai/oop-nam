﻿using System.Collections.Generic;

namespace OOP.dao
{
    class AccessoryDAO : BaseDao
    {

        /**
         * Get a list of accessories in the database filtered by name
         * @param name
         * return list
         */
        public OOP.entity.Accessory FindByName(string name)
        {
            Database database = Database.Instance();

            List<entity.Accessory> AccessoryList = database.selectTable("accessory");
            for (int i = 0; i < AccessoryList.Count; i++)
            {
                if (AccessoryList[i].name == name)
                {
                    return AccessoryList[i];
                }
            }
            return null;
        }

        /**
         * Get a list of accessories in the database filtered by category id
         * @param category id
         * return list
         */
        public List<entity.Accessory> search(int categoryId)
        {
            Database database = Database.Instance();

            List<entity.Accessory> AccessoryList = database.selectTable("accessory");

            List<entity.Accessory> FoundAccessoryList = new List<entity.Accessory>();

            for (int i = 0; i < AccessoryList.Count; i++)
            {
                if (AccessoryList[i].categotyId == categoryId)
                {
                    FoundAccessoryList.Add(AccessoryList[i]);
                }
            }
            return FoundAccessoryList;
        }

        /**
         * Delete an accessory from the database
         * @param row
         * @return true/false
         */
        public override bool Delete(dynamic row)
        {
            Database database = Database.Instance();
            database.insertTable("accessory", row);
            return true;
            //throw new System.NotImplementedException();
        }

        /**
         * Get the list of accessories from the database
         * @return list
         */
        public override dynamic FindAll()
        {
            Database database = Database.Instance();
            return database.selectTable("accessory");
            //throw new System.NotImplementedException();
        }

        /**
         * Get a list of accessories in the database filtered by id
         * @param name
         * return list
         */
        public override dynamic FindById(int id)
        {
            Database database = Database.Instance();

            List<entity.Accessory> AccessoryList = database.selectTable("accessory");
            for (int i = 0; i < AccessoryList.Count; i++)
            {
                if (AccessoryList[i].Id == id)
                {
                    return AccessoryList[i];
                }
            }
            return null;
            //throw new System.NotImplementedException();
        }

        /**
         * Store an accessory in the database
         * @param row
         * return true/false
         */
        public override int Insert(dynamic row)
        {
            Database database = Database.Instance();
            database.insertTable("accessory", row);
            return 1;
            //throw new System.NotImplementedException();
        }

        /**
         * Update an accessory to the database
         * @param row
         * return true/false
         */
        public override int Update(dynamic row)
        {
            Database database = Database.Instance();
            database.updateTable("accessory", row);
            return 1;
            //throw new System.NotImplementedException();
        }
    }
}

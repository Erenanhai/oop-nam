﻿using OOP.entity;
using System.Collections.Generic;

namespace OOP.dao
{
    internal abstract class BaseDao : IDao
    {
        public abstract bool Delete(dynamic row);
        public abstract dynamic FindAll();
        public abstract dynamic FindById(int id);
        public abstract int Insert(dynamic row);
        public abstract int Update(dynamic row);
    }
}